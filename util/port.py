import pandas as pd
import numpy as np
from io import StringIO
import os
import time

# Get port info 
def port_get(field=None, net=None, pci=None, rdma=None):
    c = os.popen("sudo mst status -v").read()
    t = pd.read_table(StringIO(c), sep='\s+', skiprows=6, dtype='string')
    t.columns = t.columns.str.lower()
    t.net = t.net.str[4:]
    # t = t.set_index('net')
    if net:
        t = t[t.net == net]
    if pci:
        t = t[t.pci == pci]
    if rdma:
        t = t[t.rdma == rdma]
    if field:
        t = t[field]
        if net or pci or rdma and type(field) == str:
            return t.iat[0]
    return t

# get port counter
def port_counter(net, field=None, nonzero=True):
    c = os.popen("ethtool -S {}".format(net)).read()
    t = pd.read_table(StringIO(c), skiprows=1, sep=':', index_col=0,
                      names=['type','counter'], skipinitialspace=True)
    if field:
        return t.loc[field][0];
    if nonzero:
        t = t[t['counter'] > 0]
    return t

last_port_counter = None
last_port_time = None

# get port counter diff and rate
def port_counter_diff(net, growing=True):
    global last_port_counter, last_port_time
    t1 = port_counter(net)
    now = time.time()
    if last_port_counter is None:
        last_port_counter = t1
        last_port_time = now
    diff = t1 - last_port_counter
    all = pd.concat([t1, diff, diff], axis=1, join='outer')
    all.columns.values[1] = 'diff'
    all.columns.values[2] = 'rate'
    if growing:
        all  = all[all['diff'] > 0]
    if last_port_time < now:
        all['rate'] = now - last_port_time
        all['rate'] = all['diff'] / all['rate']
    last_port_counter = t1
    last_port_time = now
    return all
