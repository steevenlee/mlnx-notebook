from html import escape
import time
from IPython import display
from ipywidgets import Button
import matplotlib.pyplot as plt
import numpy as np
import threading
import ipywidgets as widgets


def table(data):
    line = ["<th></th>"]
    line.extend(["<th>%s</th>"%escape(str(i)) for i in list(data.values())[0]._fields])
    rows = [line]
    for k,v in data.items():
        line = ["<th>%s</th>"%escape(str(k))]
        line.extend(["<td>%s</td>"%escape(str(i)) for i in v])
        rows.extend([line])
    s = '\n'.join(['<tr>%s</tr>'%(''.join(row)) for row in rows])
    return display.HTML('<table>%s</table>'%s)

def watch(func, timeout=1):
    try:
        while True:
            display.clear_output(wait=True)
            func()
            time.sleep(timeout)
    except KeyboardInterrupt:
        pass

################# watching chart ############
stop = True
thread = None
axes = None

def watch_stop():
    global thread, stop
    if thread and thread.isAlive():
            stop = True
            thread.join()
            thread = None

def watch_chart(plot_cb=None, nrows=2, ncols=2, refresh=1, figsize=(8,3)):
    global thread, stop, axes, fig
    
    watch_stop()

    btn = Button(description="Stop", orientation='horizontal')
    btn.layout.width = '100%'
    btn.on_click(watch_stop)
    display.display(btn)
    
    plt.close('all')
    plt.ion()
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=figsize, sharex='all', sharey='col')
    fig.canvas.header_visible = False
    for row in range(nrows):
        for col in range(ncols):
            axes[row, col].get_xaxis().set_visible(False)
            axes[row, col].spines['right'].set_visible(False)
            axes[row, col].spines['top'].set_visible(False)
    plot_cb(axes)
    fig.tight_layout()
    
    stop = False
    thread = threading.Thread(target=work, args=(refresh, plot_cb))
    thread.start()
   
def work(refresh, plot_cb):
    global stop, axes, fig
    while True:
        if stop:
            return
        plot_cb(axes)
        # fig.canvas.draw()
        # fig.canvas.flush_events()
        time.sleep(refresh)